
using JuMP

import HiGHS
import Juniper
import Ipopt
import GLPK
import LinearAlgebra as LA

import Random

function gen_model(Qs, M, Qmax, s)
    optimiser = optimizer_with_attributes(
        HiGHS.Optimizer, "presolve" => "off"
    )

    model = Model(optimiser)

    @variable(model, x[eachindex(Qs), 1:M], Bin)

    for i in 1:M
        @constraint(model, sum(x[:, i]) <= s)
        @constraint(model, LA.dot(Qs, x[:, i]) <= Qmax)
    end

    for j in eachindex(Qs)
        @constraint(model, sum(x[j, :]) == 1)
    end

    return model
end

function gen_model2(Qs, M, Qmax, s)
    # optimiser_inner = optimizer_with_attributes(
    #     Ipopt.Optimizer, "print_level" => 0
    # )
    optimiser = optimizer_with_attributes(
        HiGHS.Optimizer
        # Juniper.Optimizer, "nl_solver" => optimiser_inner
    )
    model = Model(optimiser)

    @variable(model, x[eachindex(Qs), 1:M], Bin)
    @variable(model, y[1:M], Bin)

    for i in 1:M
        @constraint(model, sum(x[:, i]) <= s * y[i])
        @constraint(model, LA.dot(Qs, x[:, i]) <= Qmax * y[i])
    end

    for i in 1:M-1
        @constraint(model, y[i+1] - y[i] <= 0)
    end

    for j in eachindex(Qs)
        @constraint(model, sum(x[j, :]) == 1)
    end

    @objective(model, Min, sum(y[1:M]))

    return model
end

function gen_model3(Qs, M, Qmax, s)
    # optimiser_inner = optimizer_with_attributes(
    #     Ipopt.Optimizer, "print_level" => 0
    # )
    optimiser = optimizer_with_attributes(
        GLPK.Optimizer
    )
    model = Model(optimiser)
    set_attribute(model, "msg_lev", GLPK.GLP_MSG_ALL)

    @variable(model, x[eachindex(Qs), 1:M], Bin)
    @variable(model, y[1:M], Bin)

    for i in 1:M-1
        @constraint(model, y[i+1] - y[i] <= 0)
    end

    for i in 1:M
        @constraint(model, sum(x[:, i]) <= s * y[i])
    end

    for j in eachindex(Qs)
        @constraint(model, sum(x[j, :]) == 1)
    end

    function powerconstr_callback(cb_data)
        status = callback_node_status(cb_data, model)
        xval = callback_value.(cb_data, x)

        for i in 1:M
            if LA.dot(Qs, xval[:, i]) > Qmax
                con = @build_constraint(sum(xval[:, i] .- xval[:, i] .* x[:, i]) >= 1)
                MOI.submit(model, MOI.LazyConstraint(cb_data), con)
            end
        end

        # if status == MOI.CALLBACK_NODE_STATUS_FRACTIONAL
        #     return
        # elseif status == MOI.CALLBACK_NODE_STATUS_INTEGER
        #     for i in 1:M
        #         if LA.dot(Qs, x[:, i]) > Qmax
        #             con = @build_constraint(sum(xval[:,i] .- xval[:,i] .* x[:,i]) >= 1)
        #             MOI.submit(model, MOI.LazyConstraint(cb_data), con)
        #         end
        #     end
        # else
        #     @assert status == MOI.CALLBACK_NODE_STATUS_UNKNOWN
        # end
    end

    @objective(model, Min, sum(y[1:M]))

    set_attribute(model, MOI.LazyConstraintCallback(), powerconstr_callback)

    return model
end

function gen_model4(Qs, M, Qmax, s)
    # optimiser_inner = optimizer_with_attributes(
    #     Ipopt.Optimizer, "print_level" => 0
    # )
    optimiser = optimizer_with_attributes(
        HiGHS.Optimizer
        # Juniper.Optimizer, "nl_solver" => optimiser_inner
    )
    model = Model(optimiser)

    @variable(model, x[eachindex(Qs), 1:M], Bin)
    @variable(model, y[1:M], Bin)

    for i in 1:M
        @constraint(model, sum(x[:, i]) <= s * y[i])
        @constraint(model, LA.dot(Qs, x[:, i]) <= Qmax * y[i])
    end

    for i in 1:M-1
        @constraint(model, y[i+1] <= y[i])
        @constraint(model, LA.dot(Qs, x[:, i+1]) <= LA.dot(Qs, x[:, i+1]))
    end

    for j in eachindex(Qs)
        @constraint(model, sum(x[j, :]) == 1)
    end

    @objective(model, Min, sum(y[1:M]))

    return model
end

function is_feasible!(m)
    optimize!(m)

    return has_values(m)
end

midpoint(i, j)::Int = floor((i + j) / 2)

function find_solution_rec(Qs, Mlower, Mupper, Qmax, s, p)
    if Mupper - Mlower == 1 || float(Mupper - Mlower) / length(Qs) < p
        return (Mlower, Mupper)
    end

    to_check = midpoint(Mlower, Mupper)

    m = gen_model(Qs, to_check, Qmax, s)

    if is_feasible!(m)
        newMlower, newMupper = Mlower, to_check
    else
        newMlower, newMupper = to_check, Mupper
    end

    return find_solution_rec(Qs, newMlower, newMupper, Qmax, s, p)
end

function find_upper_bound(Qs, Mupper, Qmax, s)
    m = gen_model(Qs, Mupper, Qmax, s)

    if is_feasible!(m)
        find_upper_bound(Qs, Mupper - 1, Qmax, s)
    else
        return Mupper + 1
    end
end

function find_solution(Qs, Qmax, s, p)
    find_solution_rec(Qs, 0, length(Qs), Qmax, s, p)
end

function find_solution2(Qs, Qmax, s)
    m = gen_model2(Qs, length(Qs), Qmax, s)
    optimize!(m)
    return objective_value(m)
end

function find_solution3(Qs, Qmax, s)
    m = gen_model3(Qs, length(Qs), Qmax, s)
    optimize!(m)
    return objective_value(m), m
end

function find_solution4(Qs, Qmax, s)
    m = gen_model4(Qs, length(Qs), Qmax, s)
    optimize!(m)
    return objective_value(m), m
end

function random_solution(Qs, Qmax, s)
    Qs = Random.shuffle(Qs)
    Q_loc = 0
    n_loc = 0
    total = 1
    for rod in Qs
        Q_loc += rod
        n_loc += 1
        if Q_loc > Qmax || n_loc > s
            total += 1
            Q_loc = rod
            n_loc = 1
        end
    end
    return total
end

function new_random_solution(Qs, Qmax, s)
    Qs = Random.shuffle(Qs)
    nrods = length(Qs)
    bin_states = zeros(nrods, 2)
    for rod in Qs
        rod_placed = false
        for bin_idx in range(1, length=nrods)
            if bin_states[bin_idx, 1] + rod <= Qmax && bin_states[bin_idx, 2] < s
                bin_states[bin_idx, :] += [rod, 1]
                rod_placed = true
                break
            end
        end
        if !rod_placed
            return -1
        end
    end
    return findfirst(isequal(0), bin_states[:, 1])-1
end

function best_random_solution(N, Qs, Qmax, s)
    current_min = length(Qs)
    for _ in 1:N
        current_min = min(current_min, random_solution(Qs, Qmax, s))
    end
    return current_min
end

# function to generate problem
function generate_problem_data(Nrods, Q_min, Q_max)
    if Q_min < 0 || Q_min > Q_max
        return -1
    end
    Qs = Random.rand(Nrods)
    Qs .*= (Q_max-Q_min)
    Qs .+= Q_min
    return Qs
end

# test loop to generate performance metrics
function compare_methods()
    Q_min = 0
    Q_max = 1
    Q_bin = 2
    s = 8
    min_nrods = 50
    max_nrods = 1000
    step_nrods = 50
    nrods_range = range(min_nrods, step=step_nrods, stop=max_nrods)
    n_runs = length(nrods_range)
    
    output = zeros(n_runs, 3)
    
    for (i, Nrods) in enumerate(nrods_range)
        Qs = generate_problem_data(Nrods, Q_min, Q_max)
        # output[i,1] = find_solution2(Qs, Q_bin, s)
        output[i,2] = random_solution(Qs, Q_bin, s)
        output[i,3] = new_random_solution(Qs, Q_bin, s)
    end
    return output
end